<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>
<?php
include "../includes/header.php";
    
extract($_GET);
$id = strip_tags($id);
require_once('./config/function.php');
$article = getArticle($id);
?>

<!DOCTYPE html>

<html>
    <head>
    <meta charset="utf-8"/>
    <title><?= $article->title ?></title>
    </head>
    <body>
    <h1><?= $article->title ?></h1>
    <p><?= $article->content ?></p>
    <?= $article->published_at?>
    <?= $article->reading_time?>
    <?= $article->firstname?>
    <?= $article->lastname?>
    <hr />










<?php
include "../includes/footer.php";
?>

