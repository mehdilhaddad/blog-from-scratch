<?php
function getArticles()
{
    require('../public/config/connect.php');
$req = $bdd->prepare('SELECT articles.id, articles.title, articles.content, articles.image_url,articles.published_at,articles.reading_time,authors.firstname,authors.lastname FROM articles JOIN authors ON author_id = authors.id ORDER BY id DESC');
$req->execute();
$data = $req->fetchALL(PDO::FETCH_OBJ);
return $data;
$req->closeCursor();
}
function getArticle($id)
{
    require('../public/config/connect.php');
    $req = $bdd->prepare('SELECT articles.id, articles.title, articles.content, articles.image_url, articles.published_at, articles.reading_time, authors.firstname, authors.lastname FROM articles JOIN authors ON author_id = authors.id WHERE articles.id = ?');
    $req->execute(array($id));
    if($req->rowCount() == 1)
    {
        $data = $req->fetch(PDO::FETCH_OBJ);
        return $data;
    }
    else{
        //header('Location: index.php');
    }
}